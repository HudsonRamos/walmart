CREATE TABLE MAPA (id int(20) NOT NULL AUTO_INCREMENT, nome varchar(255), PRIMARY KEY (id))

CREATE TABLE ROTA (id int(20) NOT NULL AUTO_INCREMENT, origem varchar(255), destino varchar(255), distancia int(20), mapa_id int(20), PRIMARY KEY (id), FOREIGN KEY (mapa_id) REFERENCES mapa(id))

INSERT INTO MAPA(nome) VALUES ('SP');

INSERT INTO ROTA(origem, destino, distancia, mapa_id) VALUES ('A','B',10,1);
INSERT INTO ROTA(origem, destino, distancia, mapa_id) VALUES ('B','D',15,1);
INSERT INTO ROTA(origem, destino, distancia, mapa_id) VALUES ('A','C',20,1);
INSERT INTO ROTA(origem, destino, distancia, mapa_id) VALUES ('C','D',30,1);
INSERT INTO ROTA(origem, destino, distancia, mapa_id) VALUES ('B','E',50,1);
INSERT INTO ROTA(origem, destino, distancia, mapa_id) VALUES ('D','E',30,1);


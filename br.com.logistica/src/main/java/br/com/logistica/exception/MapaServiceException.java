package br.com.logistica.exception;

public class MapaServiceException extends Exception {


	private static final long serialVersionUID = 5337903289953720934L;

    public MapaServiceException(String message) {
        super(message);
    }

    public MapaServiceException(Throwable cause) {
        super(cause);
    }

    public MapaServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
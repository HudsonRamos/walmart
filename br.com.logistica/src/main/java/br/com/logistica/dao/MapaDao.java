package br.com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.logistica.model.Mapa;

@Repository
public interface MapaDao extends JpaRepository<Mapa, Long> {
	
	public Mapa findByNome(String nome);		
 
}

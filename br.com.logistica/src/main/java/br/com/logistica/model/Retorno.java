package br.com.logistica.model;

import java.util.ArrayList;
import java.util.List;

public class Retorno {

	public static final String OK_STATUS = "OK";
	public static final String NOK_STATUS = "NOK";

	private String status;
	private List<String> mensagens = new ArrayList<String>();

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getMensagens() {
		return mensagens;
	}

	public void setMensagens(List<String> mensagens) {
		this.mensagens = mensagens;
	}

	@Override
	public String toString() {
		return "Retorno [status=" + status + ", mensagens=" + mensagens + "]";
	}

}

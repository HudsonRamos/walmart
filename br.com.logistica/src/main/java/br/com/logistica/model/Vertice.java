package br.com.logistica.model;

import java.util.List;

public class Vertice implements Comparable<Vertice> {

	private String nome;
	private List<Aresta> adjacentes;
	private Double distanciaMinima = Double.POSITIVE_INFINITY;
	private Vertice precedente;

	public Vertice(String nome) {
		this.nome = nome;
	}

	public int compareTo(Vertice vertice) {
		return Double.compare(getDistanciaMinima(), vertice.getDistanciaMinima());
	}

	public void setAdjacentes(List<Aresta> adjacentes) {
		this.adjacentes = adjacentes;
	}
	
	public List<Aresta> getAdjacentes() {
		return adjacentes;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getDistanciaMinima() {
		return distanciaMinima;
	}

	public void setDistanciaMinima(Double distanciaMinima) {
		this.distanciaMinima = distanciaMinima;
	}

	public Vertice getPrecedente() {
		return precedente;
	}

	public void setPrecedente(Vertice precedente) {
		this.precedente = precedente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adjacentes == null) ? 0 : adjacentes.hashCode());
		result = prime * result + ((distanciaMinima == null) ? 0 : distanciaMinima.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((precedente == null) ? 0 : precedente.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertice other = (Vertice) obj;
		if (adjacentes == null) {
			if (other.adjacentes != null)
				return false;
		} else if (!adjacentes.equals(other.adjacentes))
			return false;
		if (distanciaMinima == null) {
			if (other.distanciaMinima != null)
				return false;
		} else if (!distanciaMinima.equals(other.distanciaMinima))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (precedente == null) {
			if (other.precedente != null)
				return false;
		} else if (!precedente.equals(other.precedente))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vertice [nome=" + nome + ", adjacentes=" + adjacentes + ", distanciaMinima=" + distanciaMinima
				+ ", precedente=" + precedente + "]";
	}

}

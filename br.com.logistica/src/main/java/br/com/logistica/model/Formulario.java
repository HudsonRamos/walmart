package br.com.logistica.model;

public class Formulario {

	private String mapa;
	private String origem;
	private String destino;
	private Double consumoVeiculo;
	private Double precoCombustivel;

	public String getMapa() {
		return mapa;
	}

	public void setMapa(String mapa) {
		this.mapa = mapa;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Double getConsumoVeiculo() {
		return consumoVeiculo;
	}

	public void setConsumoVeiculo(Double consumoVeiculo) {
		this.consumoVeiculo = consumoVeiculo;
	}

	public Double getPrecoCombustivel() {
		return precoCombustivel;
	}

	public void setPrecoCombustivel(Double precoCombustivel) {
		this.precoCombustivel = precoCombustivel;
	}

	@Override
	public String toString() {
		return "Formulario [mapa=" + mapa + ", origem=" + origem + ", destino=" + destino + ", consumoVeiculo="
				+ consumoVeiculo + ", precoCombustivel=" + precoCombustivel + "]";
	}

}
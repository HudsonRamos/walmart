package br.com.logistica.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import br.com.logistica.model.Aresta;
import br.com.logistica.model.Mapa;
import br.com.logistica.model.Rota;
import br.com.logistica.model.Vertice;

public class MapaUtil {
	
	public static String rotaMaisCurta(String rotas, Double custoTotal) {
		return new StringBuffer().append("{\"rota\": \"").append(rotas).append("\", \"custoTotal\":")
				.append(custoTotal).append("}").toString();
	}
	
	public static Map<String, Vertice> getVerticeMapa(Mapa mapa) {
		
		if (mapa != null && mapa.getRotas() != null) {
			
			Map<String, Vertice> verticesMapa = new HashMap<String, Vertice>();
			Map<Vertice, ArrayList<Aresta>> verticesArestasMapa = new HashMap<Vertice, ArrayList<Aresta>>();
		
			for (Rota rota : mapa.getRotas()) {
				if (verticesMapa.get(rota.getDestino()) == null) {
					verticesMapa.put(rota.getDestino(), new Vertice(rota.getDestino()));
					verticesArestasMapa.put(verticesMapa.get(rota.getDestino()), new ArrayList<Aresta>());
				}
				if (verticesMapa.get(rota.getOrigem()) == null) {
					verticesMapa.put(rota.getOrigem(), new Vertice(rota.getOrigem()));
					verticesArestasMapa.put(verticesMapa.get(rota.getOrigem()), new ArrayList<Aresta>());
				}
				List<Aresta> arestas = (ArrayList<Aresta>) verticesArestasMapa.get( verticesMapa.get( rota.getOrigem()));
				arestas.add(new Aresta((Vertice)verticesMapa.get(rota.getDestino()), rota.getDistancia()));
			}
			
			setVerticesAdjacentes(verticesMapa, verticesArestasMapa);
			
			return verticesMapa;
		}
		return null;
		
	}
	
	private static void setVerticesAdjacentes(Map<String, Vertice> verticesMap, Map<Vertice, ArrayList<Aresta>> verticesArestasMapa) {
		for (Entry<Vertice, ArrayList<Aresta>> entry : verticesArestasMapa.entrySet()) {
			Vertice vertice = entry.getKey();
			List<Aresta> aresta = entry.getValue();
			verticesMap.get(vertice.getNome()).setAdjacentes(aresta);
		}
	}
	
	public static Double getTotalDistancia(List<Vertice> vertices, String destino) {
		for (Vertice vertice : vertices) {
			if (vertice.getNome().equals(destino)) {
				return vertice.getDistanciaMinima();
			}
		}
		return null;
	}
	
	public static Double getCustoTotal(Double distancia, Double autonomia, Double valorLitro) {
		return ((distancia / autonomia) * valorLitro);
	}
	
	public static String getRotas(List<Vertice> vertices) {
		StringBuffer sbf = new StringBuffer();
		for (Vertice vertice : vertices) {
			sbf.append(vertice.getNome() + ", ");
		}
		return sbf.toString().substring(0, sbf.toString().length() - 2);
	}
	
	public static boolean isValidaMapa (Mapa mapa) {
		if (mapa != null && !mapa.getNome().isEmpty()) {
			return true;
		}
		return false;
	}

}

package br.com.logistica.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

import br.com.logistica.model.Aresta;
import br.com.logistica.model.Vertice;

public class Dijkstra {
	
	public static void calculaCaminhos(Vertice vertice) {
		vertice.setDistanciaMinima(0.);
		PriorityQueue<Vertice> verticeQueue = new PriorityQueue<Vertice>();
		verticeQueue.add(vertice);

		while (!verticeQueue.isEmpty()) {
			Vertice vert = verticeQueue.poll();

			if (vert.getAdjacentes() != null) {
				for (Aresta aresta : vert.getAdjacentes()) {
					Vertice verticeAdjacenti = aresta.getVertice();
					Double valorAresta = aresta.getValor();
					Double distanciaTotal = vert.getDistanciaMinima() + valorAresta;
					if (distanciaTotal < verticeAdjacenti.getDistanciaMinima()) {
						verticeQueue.remove(verticeAdjacenti);
						verticeAdjacenti.setDistanciaMinima(distanciaTotal);
						verticeAdjacenti.setPrecedente(vert);
						verticeQueue.add(verticeAdjacenti);
					}
				}
			}
		}
	}

	public static List<Vertice> getCaminhoMaisCurto(Vertice verticeEncontrado) {
		List<Vertice> vertices = new ArrayList<Vertice>();
		for (Vertice vertice = verticeEncontrado; vertice != null; vertice = vertice.getPrecedente())
			vertices.add(vertice);
		Collections.reverse(vertices);
		return vertices;
	}
}
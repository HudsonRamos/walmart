
package br.com.logistica.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import br.com.logistica.dao.MapaDao;
import br.com.logistica.exception.MapaServiceException;
import br.com.logistica.model.Mapa;
import br.com.logistica.model.Vertice;

@Service
public class MapaService {
	
	@Autowired
	private MapaDao mapaDao;
	
	public Mapa findMapaByNome(String nome) throws MapaServiceException {
		return mapaDao.findByNome(nome);
	}
	
	public String calculaRotaMaisCurta(String nomeMapa, String origem, String destino, Double autonomia, Double custo) throws MapaServiceException {
		
		Mapa mapa = findMapaByNome(nomeMapa);
		
		if (MapaUtil.isValidaMapa(mapa)) {
			
			Map<String, Vertice> verticeMapa = MapaUtil.getVerticeMapa(mapa);
			
			Dijkstra.calculaCaminhos(verticeMapa.get(origem));
			List<Vertice> caminhos = Dijkstra.getCaminhoMaisCurto(verticeMapa.get(destino));
			
			Double distancia = MapaUtil.getTotalDistancia(caminhos, destino);
			String rotas = MapaUtil.getRotas(caminhos);
			Double custoTotal =  MapaUtil.getCustoTotal(distancia, autonomia, custo);
			
			return MapaUtil.rotaMaisCurta(rotas, custoTotal);
			
		} else {
			throw new MapaServiceException("Mapa nao encontrado.");
		}
	}
		
	@Transactional
	public Mapa inserirMapa(String json) throws MapaServiceException {
		
		Mapa map = converterJsonMapa(json);
		if (MapaUtil.isValidaMapa(map)) {
			Mapa mapPersist = this.findMapaByNome(map.getNome());
			map.populaRotas();
			if (mapPersist != null) map.setId(mapPersist.getId());
			return mapaDao.save(map);
		} else {
			throw new MapaServiceException("Erro na validação do mapa.");
		}
	}
	
	public Mapa converterJsonMapa(String json) {
			
			Gson gson = new Gson();
		    return gson.fromJson(json, Mapa.class);
	}
	
	public String converterMapaJson(Mapa mapa) throws MapaServiceException {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(mapa);
		} catch (JsonProcessingException e) {
			 throw new MapaServiceException("Erro ao converter Mapa.");
		 }
	}
	
}

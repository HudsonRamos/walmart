package br.com.logistica.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.logistica.exception.MapaServiceException;
import br.com.logistica.model.Formulario;
import br.com.logistica.model.Mapa;
import br.com.logistica.model.Retorno;
import br.com.logistica.service.MapaService;


@RestController
public class MapaController {
	
	
	@Autowired
    private MapaService mapaService;


    @RequestMapping(value="/inserirMapa", method = RequestMethod.GET, headers="Accept=application/json")
    public String inserirMapa(@RequestParam(value="mapa", required=false, defaultValue="") String jsonMap) {
		
    	try {
			Mapa mapa = mapaService.inserirMapa(jsonMap); 
			return mapaService.converterMapaJson(mapa);
		} catch (MapaServiceException e) {
			return getErroJson(e.getMessage());
		}
    }
    
    @RequestMapping(value="/calcular", method=RequestMethod.GET, headers="Accept=application/json")
    public String calcularRota(@ModelAttribute @Valid Formulario form, BindingResult bindingResult, HttpServletResponse response) {
		
    	try {
			if (bindingResult.hasErrors()) {
				return getErroJson(bindingResult);
			}
			return mapaService.calculaRotaMaisCurta(form.getMapa(), form.getOrigem(), form.getDestino(),
					form.getConsumoVeiculo(), form.getPrecoCombustivel());
		} catch (MapaServiceException e) {
			return getErroJson(e.getMessage());
		}
    }

    public static final String getErroJson(String mensagem) {
		 try {
			 ObjectMapper mapper = new ObjectMapper();
			 Retorno retorno = new Retorno();
			 retorno.setStatus(Retorno.NOK_STATUS);
			 retorno.getMensagens().add(mensagem);
			 return mapper.writeValueAsString(retorno);
		 } catch (JsonProcessingException e) {
			 return null;
		 }
	}
	
	public static final String getErroJson(BindingResult erros) {
		 try {
			 ObjectMapper mapper = new ObjectMapper();
			 Retorno retorno = new Retorno();
			 retorno.setStatus(Retorno.NOK_STATUS);
			 retorno.setMensagens(getErrorsAsList(erros));
			 return mapper.writeValueAsString(retorno);
		 } catch (JsonProcessingException e) {
			 return null;
		 }
	}
	
	private static List<String> getErrorsAsList(BindingResult erros) {
		if (erros != null && erros.hasErrors()) {
			List<String> listaErros = new ArrayList<String>();
			for (FieldError erro : erros.getFieldErrors()) {
				listaErros.add(erro.getField() + ":" + erro.getDefaultMessage());
			}
			return listaErros;
		}
		return null;
	}
}
Para configurar o sistema é nescessário:

- Banco de Dados Mysql (arquivo data-walmart.sql tem as DDLS e insert com os valores do exemplo)
- Servidor de Aplicação TomCat 7
- Eu rodei direto no ecplise

Caminhos:

Calcular Rota mais Curta

http://localhost:8080/br.com.logistica/calcular/?mapa=SP&origem=A&destino=E&consumoVeiculo=10&precoCombustivel=2.5

Inserir Mapa

http://localhost:8080/br.com.logistica/inserirMapa/?mapa={mapa:{nome: "SP", rotas: [{origem: "G",destino: "H",distancia: 25}]}


Foi utilizado, Spring, JPA, Hibernate, Servlet, Algoritimo do Dijkstra.

Obs: A inserção de um novo mapa não está funcionando, infelizmente não deu tempo para arrumar. 

Muito obrigado, o teste é fantástico, gostei muito e aprendi bastante. 